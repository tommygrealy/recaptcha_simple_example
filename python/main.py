from flask import Flask, request
from flask_restful import Resource, Api
import json
import time
from flask_cors import CORS
import requests
import os


app = Flask(__name__)
api = Api(app)


CORS(app)

def check_captcha(resp_code):
    google_verify_url = "https://www.google.com/recaptcha/api/siteverify"
    secret="6LcOO-sZAAAAACsTUfDMlimtbUvTgB4agTYC2oN3"
    payload={"secret":secret, "response": resp_code}
    headers = {}
    response = requests.request("POST", google_verify_url, headers=headers, data=payload)
    json_data = response.json()
    print ("Google response: " + str(json_data))
    if json_data['success']==True:
        return True
    else:
        return False

class RegisterNewUser(Resource):
    def post(self):
        if check_captcha(request.values['g-recaptcha-response']) == True:
            print(str(request.values))
            print(request.values['Name'])
            print(request.values['email'])
            print(request.values['address'])
            return ("Hi " + request.values['Name'] + ", Im going to ring you on " + request.values['address'] + " or email you at " + request.values['email'] + " as soon as possible.")
        else:
            return ("go away botty")


api.add_resource(RegisterNewUser, '/register')

app.run(host='0.0.0.0', debug=True)

